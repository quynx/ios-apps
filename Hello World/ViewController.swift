//
//  ViewController.swift
//  Hello World
//
//  Created by usama on 3/15/20.
//  Copyright © 2020 usama. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var nameImage: UIImageView!
    @IBOutlet weak var buttonShow: UIButton!
    @IBOutlet weak var backGroundImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func welcomePressed(_ sender: Any) {
    
        nameImage.isHidden = false
        buttonShow.isHidden = false
        backGroundImage.isHidden = false
    
    }
    
}

